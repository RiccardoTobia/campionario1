package org.zefiro.biblioteca3T.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.zefiro.biblioteca3T.dao.DaoFactory;

public class Configuration {

	public final static Configuration INSTANCE = new Configuration();
	
	private Properties daoProperties;
	
	private Configuration() {
		
		InputStream input = DaoFactory.class.getResourceAsStream("dao-configuration.properties");
		
		daoProperties = new Properties();
		
		if(input == null) {
			System.out.println("NON ESISTE LA CONFIGURAZIONE DEL DATA LAYER");
			return;
		}
		
		try {
			daoProperties.load(input);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public String getDaoValue(String key) {
		return daoProperties.getProperty(key);
	}
	
}
