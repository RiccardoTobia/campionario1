package org.zefiro.biblioteca3T.eccezioni;

public class LibroDaoException extends Exception{
	
	protected LibroDaoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LibroDaoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public LibroDaoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public LibroDaoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public LibroDaoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
