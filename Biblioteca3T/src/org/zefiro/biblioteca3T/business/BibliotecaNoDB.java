package org.zefiro.biblioteca3T.business;

import java.util.ArrayList;
import java.util.List;

import org.zefiro.biblioteca3T.model.Libro;

public class BibliotecaNoDB {

	private String nome;
	private int numeroLibri;
	private List<Libro> libreria;
	
	public String insertLibro(Libro l) {
		
		String result ="";
		if(l != null) {
			if(l instanceof Libro) {
			getLibreria().add(l);
			setNumeroLibri(getLibreria().size());
			result = String.format("Titolo: %s, Autore: %s", l.getTitolo(), l.getAutore());
			}
		}
		
		return result;
		
	}
	
	public void removeLibroPerIndice(int i) {
		
		if(i <= getLibreria().size()) {
			
			getLibreria().remove(i);
			setNumeroLibri(getLibreria().size());
			
		}
		
	}
	
	public boolean removeLibro(String titolo) {
		
		boolean bool = false;
		Libro l = new Libro();
		
		if(titolo != null && titolo != "") {

				for(Libro libro : getLibreria()) {
					
					if(libro.getTitolo() == titolo) {
						l = libro;
					}
					
				}
			
				if(l.getTitolo() != null && l.getTitolo() != "") {					
					getLibreria().remove(l);
					setNumeroLibri(getLibreria().size());
					
					bool = true;
				}
			
		}
		
		return bool;
		
	}
	
	// SI POTREBBE FARE ACQUISTA - PRENDI IN PRESTITO
	
	public int numeroPagineLibro(String titolo) {
		
		int pagine = 0;
		
		if(titolo != "" && titolo != null) {
			
			for (Libro libro : libreria) {
				if(libro.getTitolo() == titolo) {
					
					pagine = libro.getPagine();
					
				}
			}
			
		} else {
			System.out.println("NESSUN LIBRO TROVATO");
		}		
		
		return pagine;
		
	}
	
	//SI POTREBBE FARE TIPO TROVA PER AUTORE E ROBE SIMILI
	
	public List<Libro> trovaPerAutore(String autore){
		
		List<Libro> libriPerAutore = new ArrayList<>();
		
		if(autore != null && autore != "") {
			
			for (Libro libro : getLibreria()) {
				if(libro.getAutore().equalsIgnoreCase(autore)) {
					libriPerAutore.add(libro);
				}
			}
		}
		
		return libriPerAutore;
		
	}
	
	public List<Libro> trovaPerGenere(String genere){
		
		List<Libro> libriPerGenere = new ArrayList<>();
		
		if(genere != null && genere != "") {
			
			for (Libro libro : getLibreria()) {
				if(libro.getGenere().equalsIgnoreCase(genere)) {
					libriPerGenere.add(libro);
				}
			}
		}
		
		return libriPerGenere;
		
	}

	public List<Libro> trovaPerAutoreGenere(String autore, String genere){
		
		List<Libro> libri = new ArrayList<>();
		
		if(genere != null && genere != "" && autore != null && autore != "" ) {
			
			for (Libro libro : getLibreria()) {
				if(libro.getGenere().equalsIgnoreCase(genere) && libro.getAutore().equalsIgnoreCase(autore)) {
					libri.add(libro);
				}
			}
		}
		
		return libri;
		
	}

	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getNumeroLibri() {
		return libreria.size();
	}
	public void setNumeroLibri(int numeroLibri) {
		this.numeroLibri = numeroLibri;
	}
	public List<Libro> getLibreria() {
		if(libreria == null) {
			libreria = new ArrayList<>();
		}
		return libreria;
	}
	public void setLibreria(List<Libro> libreria) {
		this.libreria = libreria;
	}
	
	
}
