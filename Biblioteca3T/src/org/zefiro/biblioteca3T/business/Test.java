package org.zefiro.biblioteca3T.business;

import java.util.List;

import org.zefiro.biblioteca3T.eccezioni.LibroDaoException;
import org.zefiro.biblioteca3T.model.Libro;

public class Test {

	public static void main(String[] args) {

		BibliotecaService service = new BibliotecaService();
		
//		Libro l;
//		try {
//			l = new Libro();
//			l = service.findbyId(1);
//
//			if(l != null) {
//				System.out.println(String.format("Titolo: %s", l.getTitolo()));
//			} else {
//				System.out.println("ERRORRE");
//			}
//		} catch (LibroDaoException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		try {
			Libro l = new Libro();
			l.setTitolo("Gli indifferenti");
			l.setAutore("A. Moravia");
			l.setGenere("Dramma");
			l.setPagine(300);
			
			service.add(l);
			System.out.println(String.format("Aggiunto il libro: %s - Autore: %s", l.getTitolo(), l.getAutore()));
			
		} catch (LibroDaoException e) {
			e.printStackTrace();
		}
		
//		try {
//			Libro l = new Libro();
//			l.setTitolo("American Gods");
//			l.setAutore("N. Gaiman");
//			l.setGenere("Fantastico");
//			l.setPagine(400);
//			l.setId(4);
//			
//			service.update(l);
//			System.out.println(String.format("Aggiornato il libro: %s - Autore: %s", l.getTitolo(), l.getAutore()));
//			
//		} catch (LibroDaoException e) {
//			e.printStackTrace();
//		}
		
//		try {
//			
//			List<Libro> result = service.findAll();
//			
//			for (Libro libro : result) {
//				System.out.println(String.format("Titolo: %s - Autore: %s", libro.getTitolo(), libro.getAutore()));
//			}
//			
//		} catch (LibroDaoException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		BibliotecaNoDB b = new BibliotecaNoDB();
//		
//		Libro l1 = new Libro();
//		l1.setTitolo("ciaociao");
//		l1.setAutore("me");
//		l1.setGenere("me");
//		b.insertLibro(l1);
//		
//		Libro l2 = new Libro();
//		l2.setTitolo("hellohello");
//		l2.setAutore("him");
//		l2.setGenere("me");
//		b.insertLibro(l2);
//
//		Libro l3 = new Libro();
//		l3.setTitolo("byebye");
//		l3.setAutore("him");
//		l3.setGenere("him");
//		b.insertLibro(l3);
//				
//		for(Libro libro : b.trovaPerAutore("him")) {
//			
//			System.out.println(libro.getTitolo());
//		}

	}
		

}
