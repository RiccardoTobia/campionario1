package org.zefiro.biblioteca3T.business;

import java.util.List;

import org.zefiro.biblioteca3T.dao.DaoFactory;
import org.zefiro.biblioteca3T.dao.LibroDao;
import org.zefiro.biblioteca3T.eccezioni.LibroDaoException;
import org.zefiro.biblioteca3T.model.Libro;

public class BibliotecaService {

	private LibroDao dao = DaoFactory.INSTANCE.getCurrentDao();
	
	public void add(Libro l) throws LibroDaoException{
		dao.add(l);
	};
	public void delete(long id) throws LibroDaoException{
		dao.delete(id);
	};
	public void update(Libro l) throws LibroDaoException{
		dao.update(l);
	};
	
	public List<Libro> findAll() throws LibroDaoException{
		return dao.getAll();
	};
	public Libro findbyId(long id) throws LibroDaoException{
		return dao.findById(id);
	}; 
}
