package org.zefiro.biblioteca3T.business;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.zefiro.biblioteca3T.model.Libro;

public class BibliotecaNoDBUI extends JFrame {

	//North
	private JPanel pnlNorth;
	
	private JLabel lblTitolo;
	private JTextField txtTitolo;
	private JLabel lblAutore;
	private JTextField txtAutore;
	private JLabel lblGenere;
	private JTextField txtGenere;
	
	private JButton btnAggiungi; 
	private JButton btnElimina;
	private JButton btnCerca;
	private JButton btnPagine;
	
	//Center
	private JPanel pnlCenter;
	private JLabel lblPagine;
	private JList<String> libriList;
    private ListModelLibro listModel;

    private JScrollPane scrollLista;
    
    private BibliotecaNoDB service;
    
    
    public BibliotecaNoDBUI() {
    	
    	
    	service = new BibliotecaNoDB();
    	service.getLibreria();
    	
    	//North
    	pnlNorth = new JPanel(new GridLayout(5, 2));
    	
    	lblTitolo = new JLabel("Titolo :");
    	txtTitolo = new JTextField();
    	lblAutore = new JLabel("Autore :");
    	txtAutore = new JTextField();
    	lblGenere = new JLabel("Genere :");
    	txtGenere = new JTextField();
    	
    	btnAggiungi = new JButton("Aggiungi");
    	btnElimina = new JButton("Elimina");
    	btnCerca = new JButton("Cerca");
    	btnPagine = new JButton("Pagine");
    	
    	btnAggiungi.addActionListener(new GestioneBottoni());
    	btnElimina.addActionListener(new GestioneBottoni());
    	btnCerca.addActionListener(new GestioneBottoni());
    	btnPagine.addActionListener(new GestioneBottoni());

    	pnlNorth.add(lblTitolo);
    	pnlNorth.add(txtTitolo);
    	pnlNorth.add(lblAutore);
    	pnlNorth.add(txtAutore);
    	pnlNorth.add(lblGenere);
    	pnlNorth.add(txtGenere);
    	
    	pnlNorth.add(btnAggiungi);
    	pnlNorth.add(btnElimina);
    	pnlNorth.add(btnCerca);
    	pnlNorth.add(btnPagine);
    	
    	//Center
    	pnlCenter = new JPanel(new GridLayout());
    	lblPagine = new JLabel();
    	listModel = new ListModelLibro();
    	libriList = new JList<String>(listModel);
    	scrollLista = new JScrollPane(libriList);
    	
    	pnlCenter.add(lblPagine);
    	pnlCenter.add(libriList);
    	pnlCenter.add(scrollLista);
    	
    	getContentPane().setLayout(new BorderLayout());
    	getContentPane().add(pnlNorth, BorderLayout.NORTH);
    	getContentPane().add(pnlCenter, BorderLayout.CENTER);

    	setTitle("Biblioteca 1.0");
    	setSize(300, 500);
    }
    
    
    public void onClickAggiungi() {
    	
    	Libro l = new Libro();
    	
    	l.setTitolo(txtTitolo.getText());
    	l.setAutore(txtAutore.getText());
    	l.setGenere(txtGenere.getText());

    	String rs = service.insertLibro(l);
    	
    	if(rs != "" || rs != null) {

			txtAutore.setText("");
			txtGenere.setText("");				
			txtTitolo.setText("");
    		
    		listModel.addRow(rs);
    	} else {
    		JOptionPane.showMessageDialog(this, "ERRORE IN INSERIMENTO");
    	}
    	
    }
    
    public void onClickElimina() {
    	
    	String titolo = txtTitolo.getText();
    	
    	
    	boolean bool = service.removeLibro(titolo);
    	
    	if(bool == true) {
    		
    		listModel.clearAll();
    		
			for (Libro libro : service.getLibreria()) {
					
				listModel.addRow(String.format("Titolo: %s, Autore: %s", libro.getTitolo(), libro.getAutore()));

				
    			JOptionPane.showMessageDialog(this, "Tutto bene!");
			}
    	
    	} else {
    		JOptionPane.showMessageDialog(this, "ERRORE!");
    		
    	}
    }
    
    public void onClickPagina() {
    	
    	String titolo = txtTitolo.getText();
    	
    	int pagine = service.numeroPagineLibro(titolo);

    	lblPagine.setText(Integer.toString(pagine));
    	
    }
    
    public void onClickCercaAutore() {
    	
    	String autore = txtAutore.getText();
    	
		
    	if(service.getLibreria().size() > 0) {

    		listModel.clearAll();

    		for(Libro libro : service.trovaPerAutore(autore)) {
    			
				listModel.addRow(String.format("Titolo: %s, Autore: %s", libro.getTitolo(), libro.getAutore()));
        		
        	}	
    	} else {
    		JOptionPane.showMessageDialog(this, "NESSUN LIBRO TROVATO CON QUELL'AUTORE");
    	}
    	
    }

    public void onClickCercaGenere() {
    	
    	String genere = txtGenere.getText();
    	
    	List<Libro> libri = service.trovaPerGenere(genere);
    	
    	for(Libro libro : libri) {
    		System.out.println(libro.getTitolo());
    	}
    	
    	if(libri.size() > 0) {

    		listModel.clearAll();

    		for(Libro libro : libri) {
    			
				listModel.addRow(String.format("Titolo: %s, Autore: %s", libro.getTitolo(), libro.getAutore()));
        		
        	}	
    	} else {
    		JOptionPane.showMessageDialog(this, "NESSUN LIBRO TROVATO CON QUEL GENERE");
    	}
    	
    }
    
    public void onClickCercaAutoreGenere() {
    	
    	String autore = txtAutore.getText();
    	String genere = txtGenere.getText();
    	
    	List<Libro> libri = service.trovaPerAutoreGenere(autore, genere);
    	
    	if(libri.size() > 0) {

    		listModel.clearAll();

    		for(Libro libro : libri) {
    			
				listModel.addRow(String.format("Titolo: %s, Autore: %s", libro.getTitolo(), libro.getAutore()));
        		
        	}	
    	} else {
    		JOptionPane.showMessageDialog(this, "NESSUN LIBRO TROVATO CON QUEL AUTORE GENERE");
    	}
    	
    }

    class GestioneBottoni implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if(e.getSource() == btnAggiungi) {
				onClickAggiungi();
			}
			
			if(e.getSource() == btnElimina) {
				onClickElimina();
			}
			
			if(e.getSource() == btnPagine) {
				onClickPagina();
			}
			
			if(e.getSource() == btnCerca) {
				
				if(txtAutore.getText().length() > 0 && txtGenere.getText().length() > 0) {
					onClickCercaAutoreGenere();
				} else {
					if(txtAutore.getText().length() > 0) {
						onClickCercaAutore();
					}
					
					if(txtGenere.getText().length() > 0) {
						onClickCercaGenere();
					}
				}
				
			}
		}}
    
    
}
