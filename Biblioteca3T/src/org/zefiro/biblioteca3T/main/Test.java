package org.zefiro.biblioteca3T.main;

import java.util.ArrayList;
import java.util.List;

import org.zefiro.biblioteca3T.dao.postgre.PostgreLibroDao;
import org.zefiro.biblioteca3T.eccezioni.LibroDaoException;
import org.zefiro.biblioteca3T.model.Libro;

public class Test {

	public static void main(String[] args) {

		PostgreLibroDao dao = new PostgreLibroDao();
		//TEST 1: AGGIUNGERE AL DATABASE
//		
//		
//		Libro l = new Libro();
//		
//		l.setTitolo("American Gods");
//		l.setAutore("Neil Gaiman");
//		l.setGenere("Fantastico");
//		l.setPagine(400);
//		
//		try {
//			dao.add(l);
//			
//			System.out.println("Tutto a posto");
//		} catch (LibroDaoException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			
//			System.out.println("Non tutto a posto");
//		}
//		
		
		// TEST 2: ELIMINAZIONE
		
//		try {
//			dao.delete(3);
//			System.out.println("Oke");
//		
//		} catch (LibroDaoException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			System.out.println("NO OKE");
//		}
		
		//TEST 3: UPDATE
		
//		try {
//		
//			Libro l = new Libro();
//			
//			l.setId(1);
//			l.setTitolo("Il signore degli anelli: le due torri");
//			l.setAutore("J.R.R. Tolkien");
//			l.setGenere("Fantasy Epico");
//			l.setPagine(650);
//			
//			dao.update(l);
//			
//			System.out.println("OKE");
//			
//		} catch (Exception e) {
//		
//			e.printStackTrace();
//			
//			System.out.println("NO OKE");
//		}
//
		
		//TEST4: FINDBYID
//		try {
//			Libro l = dao.findById(1);
//			if(l != null) {
//				System.out.println("Libro trovato");
//				System.out.println(String.format("Titolo: %s, Autore: %s", l.getTitolo(), l.getAutore()));
//			} else {
//				System.out.println("Libro non trovato");
//			}
//			System.out.println("OKE");
//		} catch (LibroDaoException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			System.out.println("NO OKE");
//		}
		
		
		//TEST 5
//		try {
//			List<Libro> l = new ArrayList<>();
//			l = dao.getAll();
//			
//			if(l != null) {
//				
//				System.out.println("Libri trovati");
//				for (Libro libro : l) {
//					System.out.println(String.format("Titolo: %s, Autore: %s", libro.getTitolo(), libro.getAutore()));					
//				}
//			} else {
//				System.out.println("Libri non trovati");
//			}
//			
//		} catch (LibroDaoException e) {
//			e.printStackTrace();
//			System.out.println("NO OKE");
//		}
	}

}
