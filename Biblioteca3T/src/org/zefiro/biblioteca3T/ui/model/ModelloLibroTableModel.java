package org.zefiro.biblioteca3T.ui.model;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.zefiro.biblioteca3T.business.BibliotecaService;
import org.zefiro.biblioteca3T.eccezioni.LibroDaoException;
import org.zefiro.biblioteca3T.model.Libro;

public class ModelloLibroTableModel extends AbstractTableModel {

	private List<Libro> righe;
	private BibliotecaService service = new BibliotecaService();
	protected ModelloLibroTableModel() {
		try {
			righe = service.findAll();
		} catch (LibroDaoException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getRowCount() {
		return righe.size();
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		Libro l = righe.get(rowIndex);
		
		switch (columnIndex) {
		case 0: {	
			
			return l.getTitolo();
		}
		case 1: {	
			
			return l.getAutore();
		}
		case 3: {	
			
			return l.getGenere();
		}
		case 4: {	
			
			return l.getPagine();
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + columnIndex);
		}
	}
	
	@Override
	public String getColumnName(int column) {

		switch (column) {
		case 0: {
			
			return "Titolo";
		}
		
		case 1: {
			
			return "Autore";
		}
		
		case 2: {
			
			return "Genere";
		}
		
		case 3: {
			
			return "Pagine";
		}
		
		default:
			throw new IllegalArgumentException("Unexpected value: " + column);
		}
	}
	
	public Libro getPersonaAt(int row) {
		
		return righe.get(row);
		
	}
	
	/**
	 *  Metodo che permette di rileggere i dati del db, senn� qualsiasi cosa faccio a quei dati
	 *  non vedo i cambiamenti.
	 */
	
	
	public void refreshDataGrid() {
		
		try {
			righe = service.findAll(); // Eseguo quello che si chiama "roundTrip" sul db.
			fireTableDataChanged();
		} catch (LibroDaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}

}
