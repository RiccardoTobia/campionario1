package org.zefiro.biblioteca3T.dao.postgre;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.zefiro.biblioteca3T.configuration.Configuration;
import org.zefiro.biblioteca3T.dao.LibroDao;
import org.zefiro.biblioteca3T.eccezioni.LibroDaoException;
import org.zefiro.biblioteca3T.model.Libro;

public class PostgreLibroDao implements LibroDao{


	
	private Connection connection;
	private PreparedStatement insert;
	private PreparedStatement delete;
	private PreparedStatement update;
	private PreparedStatement findById;
	
	public void add(Libro l) throws LibroDaoException{
		
		try {
			getInsert().clearParameters();
			
			getInsert().setString(1, l.getTitolo());
			getInsert().setString(2, l.getAutore());
			getInsert().setString(3, l.getGenere());
			getInsert().setInt(4, l.getPagine());
			
			getInsert().execute();
			
		} catch (SQLException ex) {
				ex.printStackTrace();
				
				throw new LibroDaoException(
						"Errore nell'insermiento del libro"
						);
		}
	};
	
	public void delete(long id) throws LibroDaoException {
		
		try {
			getDelete().clearParameters();
			
			getDelete().setLong(1,id);
			
			getDelete().execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
			
			throw new LibroDaoException("Errore nella rimozione del libro");
		}
	};
	
	public void update(Libro l) throws LibroDaoException {
		
		try {
			getUpdate().clearParameters();
			
			getUpdate().setString(1,l.getTitolo());
			getUpdate().setString(2, l.getAutore());
			getUpdate().setString(3,l.getGenere());
			getUpdate().setInt(4, l.getPagine());
			getUpdate().setLong(5, l.getId());
			
			getUpdate().execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new LibroDaoException("Errore nell'update");
		}
		
	};
	
	public Libro findById(long id)throws LibroDaoException {
		
		Libro result = null;
		try {
			getFindById().clearParameters();
			getFindById().setLong(1, id);
			
			ResultSet rs = getFindById().executeQuery();
			
			if(rs.next()) {
				
				result = new Libro();
				
				result.setId(rs.getLong("id"));
				result.setTitolo(rs.getString("titolo"));
				result.setAutore(rs.getString("autore"));
				result.setGenere(rs.getString("genere"));
				result.setPagine(rs.getInt("pagine"));
			}
			
			
		} catch (SQLException e) {

			e.printStackTrace();
			throw new LibroDaoException("Errore in findById", e);
		}
		return result;
	};
	
	public List<Libro> getAll() throws LibroDaoException {
		
		List<Libro> result = new ArrayList<>();
		
		try {
			ResultSet rs = getConnection().createStatement().executeQuery("select * from libro");
			
			while(rs.next()) {
				
				Libro l = new Libro();
				
				l.setId(rs.getLong("id"));
				l.setTitolo(rs.getString("titolo"));
				l.setAutore(rs.getString("autore"));
				l.setGenere(rs.getString("genere"));
				l.setPagine(rs.getInt("pagine"));
				
				result.add(l);
				
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
			throw new LibroDaoException("Errore in FindAll");
		}
		
		return result;
	}
	

	public Connection getConnection() throws SQLException{ 
		if(connection == null) {
			connection = DriverManager.getConnection(Configuration.INSTANCE.getDaoValue("connectionString"));
		}
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public PreparedStatement getInsert() throws SQLException{
		
		if(insert == null) {
			insert = getConnection().prepareStatement(
					"insert into libro (titolo, autore, genere, pagine) values(?, ?, ?, ?)"
					);
		}
		
		return insert;
	}

	public void setInsert(PreparedStatement insert) {
		this.insert = insert;
	}

	public PreparedStatement getDelete() throws SQLException {
		
		if(delete == null) {
			delete = getConnection().prepareStatement(
					"delete from libro where id=?"
					);
		}
		
		return delete;
	}

	public void setDelete(PreparedStatement delete) {
		this.delete = delete;
	}

	public PreparedStatement getUpdate() throws SQLException{
		
		if(update == null) {
			
			update = getConnection().prepareStatement(
					"update libro set titolo=?, autore=?, genere=?, pagine=? where id = ?"
					);
			
		}
		return update;
	}

	public void setUpdate(PreparedStatement update) {
		this.update = update;
	}

	public PreparedStatement getFindById() throws SQLException {
		if(findById == null){
			findById = getConnection().prepareStatement("select * from libro where id=?");
		}
		return findById;
	}

	public void setFindById(PreparedStatement findById) {
		this.findById = findById;
	}

}
