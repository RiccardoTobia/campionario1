package org.zefiro.biblioteca3T.dao;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.zefiro.biblioteca3T.configuration.Configuration;

public class DaoFactory {

	public static final DaoFactory INSTANCE = new DaoFactory();
	
	private Map<String, Object> cacheDaos;
	
	private DaoFactory() {
		cacheDaos = new HashMap<>();
	}
	
	public LibroDao getCurrentDao() {
		
//		
//		// Prende da file configurazione il tipo di dao, e lo scrive.
//		// Esempio factory statica	
//		String daoType = Configuration.INSTANCE.getDaoValue("daoType");
//		
//		if(daoType.equals("Postgres")) 
//		    return new PostgrePersonaDao();
//		
//		if(daoType.equals("File"))
//			return new FilePersonaDao();
//		
//		
//		return null; // Se non c'� il dao.


		if(cacheDaos.containsKey(LibroDao.class.getName())) {
			return (LibroDao)cacheDaos.get(LibroDao.class.getName());
		} else {
			String daoType = Configuration.INSTANCE.getDaoValue(LibroDao.class.getName());
			
			try {
				Class clazz = Class.forName(daoType);
				
				LibroDao dao = (LibroDao) clazz.getDeclaredConstructor().newInstance(); 
				
				cacheDaos.put(LibroDao.class.getName(), dao);
				
				return dao;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	    throw new RuntimeException(new Error("Impossibile fornire una implementazione all'interfaccia richiesta"));
	}
}
