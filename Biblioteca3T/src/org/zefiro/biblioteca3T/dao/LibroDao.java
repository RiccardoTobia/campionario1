package org.zefiro.biblioteca3T.dao;

import java.util.List;

import org.zefiro.biblioteca3T.eccezioni.LibroDaoException;
import org.zefiro.biblioteca3T.model.Libro;

public interface LibroDao {

	public void add(Libro l) throws LibroDaoException;
	public void delete(long id) throws LibroDaoException;
	public void update(Libro l) throws LibroDaoException;
	public Libro findById(long id)throws LibroDaoException;
	public List<Libro> getAll() throws LibroDaoException;
	
}
